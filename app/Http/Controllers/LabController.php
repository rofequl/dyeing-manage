<?php

namespace App\Http\Controllers;

use App\Grey;
use App\Lab;
use App\Order_list;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class LabController extends Controller
{
    public function index()
    {
        if (!user_has_permission(Auth::user()->id, 4)) abort(404);
        $lab = Order_list::where('lab_status', 1)->orderBy('id', 'DESC')->get();
        return view('lab', compact('lab'));
    }

    public function newLab()
    {
        if (!user_has_permission(Auth::user()->id, 4)) abort(404);
        return view('lab_received');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        if (!user_has_permission(Auth::user()->id, 4)) abort(404);
        $request->validate([
            'lab_name' => 'required|max:255',
            'id' => 'required',
        ]);


        $order = Order_list::findOrFail($request->id);
        $order->lab_status = 1;
        $order->lab_name = $request->lab_name;
        $order->save();


        Session::flash('message', 'Lab entry successfully');
        return redirect('Lab');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
