<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use App\RoleHasPermission;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Session;

class UserController extends Controller
{
    public function index()
    {
        $user = User::findOrFail(Auth::user()->id);
        return view('auth.profile', compact('user'));
    }

    public function UserManage()
    {
        if (!user_has_permission(Auth::user()->id, 1)) abort(404);
        $user = user::all();
        $role = Role::all();
        return view('auth.user_manage', compact('user', 'role'));
    }

    public function role()
    {
        if (!user_has_permission(Auth::user()->id, 1)) abort(404);
        $role = Role::all();
        return view('auth.role', compact('role'));
    }

    public function RoleStore(Request $request)
    {
        if (!user_has_permission(Auth::user()->id, 1)) abort(404);
        $request->validate([
            'name' => 'required|max:191|unique:roles,name',
        ]);

        $insert = new role();
        $insert->name = $request->name;
        $insert->save();

        Session::flash('message', 'Role name add successfully');
        return redirect('role');
    }

    public function RoleDestroy($id)
    {
        if (!user_has_permission(Auth::user()->id, 1)) abort(404);

        $role_has_permission = RoleHasPermission::where('role_id', $id)->get();
        $user = user::where('role_id', $id)->get();
        if ($role_has_permission->count() > 0 && $user->count() > 0) {
            return redirect()->back()->withErrors(['message' => ['Role already use, you can\'t delete']]);
        }

        $role = role::findOrFail($id);
        $role->delete();

        Session::flash('message', 'Role delete successfully');
        return redirect('role');
    }

    public function RoleEdit($id)
    {
        if (!user_has_permission(Auth::user()->id, 1)) abort(404);
        $edit = role::findOrFail($id);
        $role = role::orderBy('id', 'DESC')->get();
        return view('auth.role', compact('role', 'edit'));
    }

    public function RoleUpdate(Request $request, $id)
    {
        if (!user_has_permission(Auth::user()->id, 1)) abort(404);
        $request->validate([
            'name' => 'required|max:191|unique:roles,name,' . $id,
        ]);

        $role = role::findOrFail($id);
        $role->name = $request->name;
        $role->save();

        Session::flash('message', 'Role name update successfully');
        return redirect('role');
    }

    public function RolePermission()
    {
        if (!user_has_permission(Auth::user()->id, 1)) abort(404);
        $role = role::all();
        $permission = Permission::all();
        return view('auth.permission', compact('role', 'permission'));
    }

    public function RolePermissionStore(Request $request)
    {
        if (!user_has_permission(Auth::user()->id, 1)) abort(404);
        if ($request->action == 'add') {
            $insert = new RoleHasPermission();
            $insert->role_id = $request->roleId;
            $insert->permission_id = $request->permissionId;
            $insert->save();
            return 'save';
        } else {
            $data = RoleHasPermission::where('role_id', $request->roleId)->where('permission_id', $request->permissionId)->first();
            $data->delete();
            return 'delete';
        }
    }

    public function ProfileEdit($id)
    {
        if (!user_has_permission(Auth::user()->id, 1)) abort(404);
        $user = user::findOrFail(base64_decode($id));
        $role = role::all();
        return view('auth.profile_edit', compact('user', 'role'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255|email|unique:users,email,' . base64_decode($id),
            'phone' => 'max:26',
            'address' => 'max:255',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5000'
        ]);

        $user = user::findOrFail(base64_decode($id));
        if ($request->hasFile('image')) {
            if (File::exists($user->image) && 'assets/images/user.png' != $user->image) {
                File::delete($user->image);
            }
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileStore3 = rand(10, 100) . time() . "." . $extension;
            $request->file('image')->storeAs('public/user', $fileStore3);
            $image = 'storage/user/' . $fileStore3;
        } else {
            $image = $user->image;
        }

        User::findOrFail(base64_decode($id))->update([
            'name' => $request['name'],
            'email' => $request['email'],
            'address' => $request['address'],
            'phone' => $request['phone'],
            'image' => $image,
        ]);

        Session::flash('message', 'Profile update successfully');
        return redirect()->back();
    }

    protected function ProfilePassword(Request $request, $id)
    {
        $request->validate([
            'old_password' => 'required|string|min:8',
            'password' => 'required|string|min:8|confirmed',
        ]);

        $user = User::findOrFail(base64_decode($id));
        if (Hash::check($request->old_password, $user->password)) {
            $user->update([
                'password' => Hash::make($request['password']),
            ]);
            Session::flash('message', 'Password change successfully');
            return redirect()->back();
        } else {
            return redirect()->back()->withErrors(['message' => ['Old password not match']]);
        }
    }

    public function destroy($id)
    {
        //
    }
}
